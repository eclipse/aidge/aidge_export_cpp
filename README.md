# Aidge CPP Export

Use this module to export your Aidge model to a generic CPP export

## Install

Install with:

    pip install -v .

## Development mode install

For editable/development mode, install with:

    pip install -v --no-build-isolation -e .
