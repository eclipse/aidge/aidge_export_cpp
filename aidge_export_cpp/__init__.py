r"""
Aidge Export for CPP standalone projects

"""
from .utils import ROOT
from .export_registry import ExportLibCpp
from .operators import *
from collections import defaultdict
from .export import *

