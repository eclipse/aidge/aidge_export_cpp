# Examples on how to use this module Aidge CPP Export

This folder contains some examples on how to use the `Aidge CPP Export` module in your projects.
- [LeNet CPP export for MNIST dataset](./export_LeNet/)

Feel free to propose your own contributions with this module !