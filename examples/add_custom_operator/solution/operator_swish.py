from aidge_export_cpp.operators import *

# Use this to add Swish operator in the notebook
@operator_register("Swish")
class SwishCPP(ExportNode):
    def __init__(self, node):
        super().__init__(node)

        self.betas = self.operator.get_attr("betas")

        if len(self.outputs_dims[0]) == 4:
            # Case : Swish is after a convolution
            # outputdims = [batch, nb_outputs, Height, Width]
            # transform to [nb_outputs, Height, Width]
            self.outputs_dims[0] = self.outputs_dims[0][1:]

        if len(self.outputs_dims[0]) == 2:
            # Case : Swish is after a Fully Connected
            # outputdims = [batch, nb_outputs]
            # transform to [nb_outputs, 1, 1]
            self.outputs_dims[0] = [self.outputs_dims[0][1], 1, 1]

    def export(self, export_folder:str, list_configs:list):

        copyfile("for_export/swish_kernel.hpp",
                 f"{export_folder}/include/kernels/")

        list_configs.append(f"layers/{self.name}.h")
        list_configs.append(f"include/kernels/swish_kernel.hpp")
        generate_file(
            f"{export_folder}/layers/{self.name}.h",
            "for_export/swish_config.jinja",
            name=self.name,
            output_dims=self.outputs_dims[0],
            betas=self.betas,
            beta_type="float"
            )

        return list_configs

    def forward(self, list_actions:list):

        if not self.is_last:
            list_actions.append(set_up_output(self.name, "float"))

        list_actions.append(generate_str(
            "for_export/swish_forward.jinja",
            name=self.name,
            input_name=self.inputs[0].name(),
            output_name=self.name
        ))
        return list_actions